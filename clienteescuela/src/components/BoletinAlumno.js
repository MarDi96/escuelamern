import React, { useState, useEffect } from "react";
import axios from "axios";

const BoletinAlumno = () => {
  const [alumnos, setAlumnos] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    //cada vez que un state cambie refresco
    fetchAlumnos();
  }, []);

  const fetchAlumnos = async () => {
    try {
      // get a backend
      const response = await axios.get("/alumno");
      setAlumnos(response.data);
    } catch (error) {
      setError(error.message);
    }
  };

  const handlePrintClick = (id) => {
    const alumnox = alumnos.find((alumno) => alumno._id === id);
  
    const printWindow = window.open("", "_blank");
  
    const content = `
      <html>
        <head>
          <title>Boletín de ${alumnox.nombre}</title>
          <style>
            body {
              font-family: Arial, sans-serif;
            }
            table {
              border-collapse: collapse;
              width: 100%;
            }
            th, td {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }
            th {
              background-color: #f2f2f2;
            }
          </style>
        </head>
        <body>
          <h2>Boletín de ${alumnox.nombre}</h2>
          <p>DNI: ${alumnox.dni}</p>
          <p>Legajo: ${alumnox.legajo}</p>
          <p>Curso: ${alumnox.curso && alumnox.curso.nombre}</p>
          <table>
            <thead>
              <tr>
                <th>Materia</th>
                <th>Calificación</th>
              </tr>
            </thead>
            <tbody>
              ${alumnox.notas.map((nota) => `
                <tr>
                  <td>${nota.materia.nombre}</td>
                  <td>${nota.calificacion}</td>
                </tr>
              `).join('')}
            </tbody>
          </table>
        </body>
      </html>
    `;
  
    printWindow.document.write(content);
    printWindow.document.close();
  };
  

  return (
    <div>
    {error && <p>Error: {error}</p>}
      <ul className="ul-sin-bullets">
        {alumnos.map((alumno) => (
            <li key={alumno._id}>
              <div className="infobox">
                <p>Nombre: {alumno.nombre}</p>
                <p>DNI: {alumno.dni}</p>
                <p>Legajo: {alumno.legajo}</p>
                <p>Curso: {alumno.curso && alumno.curso.nombre}</p>
                <ul>
                  {alumno.notas.map((nota) => (
                    <li key={nota.materia._id}>
                      {nota.materia.nombre}:&nbsp;
                        <span>{nota.calificacion}</span>                     
                    </li>
                  ))}
                </ul>
                <button onClick={() => handlePrintClick(alumno._id)}>
                  Imprimir boletín
                </button>
              </div>
            </li>
          )
        )}
      </ul>
    </div>
  );
};

export default BoletinAlumno;
