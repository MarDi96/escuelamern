import React, { useState, useEffect } from "react";
import axios from "axios";

const EdicionCurso = () => {
  const [cursos, setCursos] = useState([]);
  const [materias, setMaterias] = useState([]);
  const [error, setError] = useState(null);
  const [editingCurso, setEditingCurso] = useState(null);
  const [editedValues, setEditedValues] = useState({});

  useEffect(() => {
    //cargo los arrays para después mapear
    fetchCursos();
    fetchMaterias();
  }, []); //array vacío para evitar infinite loop

  const fetchCursos = async () => {
    try {
      const response = await axios.get("/curso");
      setCursos(response.data);
    } catch (error) {
      setError("Falló la obtención de cursos (" + error.message + ")");
      console.error("Error haciendo get de cursos: " + error.message);
    }
  };

  const fetchMaterias = async () => {
    try {
      const response = await axios.get("/materia");
      setMaterias(response.data);
    } catch (error) {
      setError("Falló la obtención de materias (" + error.message + ")");
      console.error("Error haciendo get de materias: " + error.message);
    }
  };

  const handleEditClick = (curso) => {
    setEditingCurso(curso);
    setEditedValues({
        nombre: curso.nombre,
        materias: [...curso.materias],
      });
  };

  const handleSaveClick = async (id) => {
    try {
      await axios.patch(`/curso/${id}`, editedValues);
      setEditingCurso(null);
      fetchCursos();
      setError("");
    } catch (error) {
      setError("Error de edición: " + error.message);
    }
  };

  const handleInputChange = (e) => {
    const { nombre, value } = e.target;
    setEditedValues((prevValues) => ({
      ...prevValues,
      [nombre]: value,
    }));
  };

  return (
    <div>
      {error && <p>Error: {error}</p>}
      <ul className="ul-sin-bullets">
        {cursos.map((curso) => (
          <li key={curso._id}>
            {editingCurso && editingCurso._id === curso._id ? (
              <div className="curso--edicion-formbox">
                <label className="curso--nombre-box">
                    Curso:
                <input
                  type="text"
                  name="nombre"
                  value={editedValues.nombre}
                  onChange={handleInputChange}
                />
                </label>
                <label>
                  Materias:
                  <select
                    multiple
                    name="materias"
                    value={editedValues.materias}
                    onChange={(e) =>
                      setEditedValues((prevValues) => ({
                        ...prevValues,
                        materias: Array.from(
                          e.target.selectedOptions,
                          (option) => option.value
                        ),
                      }))
                    }
                  >
                    {materias.map((materia) => (
                      <option key={materia._id} value={materia._id}>
                        {materia.nombre}
                      </option>
                    ))}
                  </select>
                </label>
                <button className='curso--botonsubmit' onClick={() => handleSaveClick(curso._id)}>
                  Guardar
                </button>
                <button className="curso--botoncancelar" onClick={() => handleEditClick(null)}>
                Cancelar
              </button>
              </div>
            ) : (
              <div className="infobox">
                <p>Nombre: {curso.nombre}</p>
                <p>Materias: {curso.materias.map((materia) => <div key={materia._id}>{materia.nombre}</div>)}</p>
                <button onClick={() => handleEditClick(curso)}>Editar</button>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default EdicionCurso;
