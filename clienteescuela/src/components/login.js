// Login.js
import React, {useState} from 'react';
import axios from 'axios';

const Login = ({setUser}) => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errors, setErrors] = useState('')

  const submitLogin = (e) =>{
    e.preventDefault()
    setErrors('')
    axios.post('/auth/login', {username:username,password:password})
    .then(res=> {
       localStorage.setItem('jwtToken', res.data.token)
       axios.defaults.headers.common['Authorization'] =    
         'Bearer'+res.data.token
       setUser({ auth:true, username: res.data.username })
       
    })
    .catch(err=>{
       if(err.response){
         if(err.response.status===401) setErrors('Invalid credentials')
         else setErrors('Please try again.')
    }
       console.log(err)
    })
  }

  return (<main>
    <form onSubmit={e=> submitLogin(e)}>
    <div className='loginbox'>
    {/* User */}
    <div className='login'>
    <label htmlFor="username">
        Usuario
      </label>
      <input type="text" value={username} className='textbox' placeholder='usuario' onChange = {e=> setUsername(e.target.value)}/>

    </div>

    {/* Password */}
    <div className="login">
    <label htmlFor="password">
        Password
      </label>
      <input type="password" value={password} className='textbox' placeholder='password' onChange = {e=> setPassword(e.target.value)}/>

      </div>
        {/* Login */}
        <button type="submit" className='botonlogin'>
          Login
        </button>
        <p>{errors}</p>
      </div>
      </form>
      
  </main>);
};

export default Login;
