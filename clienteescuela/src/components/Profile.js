import React from "react";
import axios from "axios";
import Update from "./Update";
import Header from "./Header";

const Profile = ({ user, setUser }) => {
  const handleLogout = (e) => {
    e.preventDefault(e);
    axios.get("/auth/logout")
      .then((res) => {
        localStorage.removeItem("jwtToken");
        delete axios.defaults.headers.common["Authorization"];
        setUser({ auth: false, username: "" });
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <Header user={user} setUser={setUser}/>
      <p>{`Hi ${user.username}`}</p>
      
      

      <button type="submit" onClick={(e) => handleLogout(e)}>
        Logout
      </button>
    </div>
  );
};

export default Profile;

//<Update />
