import React, {useState} from 'react';
import Header from './Header';
import CreacionCurso from './CreacionCurso';
import EdicionCurso from './EdicionCurso';

const ManejoCurso = () => {
    const [showCrear, setShowCrear] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    
      return (
        <div>
          <Header />
          <button onClick={() => setShowCrear(!showCrear)}>
            Registro de cursos nuevos
          </button>
          
    
          <button onClick={() => setShowEdit(!showEdit)}>
            Lista de cursos
          </button>
          
    
          <div>
          {showCrear ? <CreacionCurso /> : null}
          {showEdit ? <EdicionCurso /> : null}
          </div>
        </div>
      );
    
    
};

export default ManejoCurso;