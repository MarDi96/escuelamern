import React, {useState} from 'react';
import axios from 'axios';

const CreacionMateria = () => {
    const [formData, setFormData] = useState({
        nombre: '',
      });
      const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
      };

      const registroMateria = async () => {
        try {
          // Make a POST request to the specified route
          const response = await axios.post('/materia', formData);
          
          // Handle the response as needed
          console.log('Registro completo:', response.data);
        } catch (error) {
          // Handle errors
          console.error('Error de registro:', error);
        }
      };
    return(
        <div>
        <form className="formbox" onSubmit={registroMateria}>
        <h1>Registro de materia</h1>
          <input
            type="text"
            name="nombre"
            className="textbox"
            onChange={handleChange}
            placeholder="Nombre de materia"
          />
          <button type="submit" className="botonregistrar">
            Registrar
          </button>
        </form>
        </div>
        );
}

export default CreacionMateria;