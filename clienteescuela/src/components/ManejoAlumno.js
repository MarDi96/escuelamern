import React, {useState} from "react";

import Header from './Header';
import CreacionAlumno from "./CreacionAlumno";
import EdicionAlumno from "./EdicionAlumno";
import NotasAlumno from "./NotasAlumno";
import BoletinAlumno from "./BoletinAlumno";

const ManejoAlumno = () => {
const [showCrear, setShowCrear] = useState(false);
const [showEdit, setShowEdit] = useState(false);
const [showNotas, setShowNotas] = useState(false);
const [showBoletin, setShowBoletin] = useState(false);

  return (
    <div>
      <Header />
      <button onClick={() => setShowCrear(!showCrear)}>
        Registro de alumnos nuevos
      </button>
      

      <button onClick={() => setShowEdit(!showEdit)}>
        Edicion de alumnos
      </button>

      <button onClick={() => setShowNotas(!showNotas)}>
        Edicion de notas
      </button>

      <button onClick={() => setShowBoletin(!showBoletin)}>
        Boletines de alumnos
      </button>
      

      <div>
      {showCrear ? <CreacionAlumno /> : null}
      {showEdit ? <EdicionAlumno /> : null}
      {showNotas ? <NotasAlumno /> : null}
      {showBoletin ? <BoletinAlumno /> : null}
      </div>
    </div>
  );
};

export default ManejoAlumno;


/*
const [cursos, setCursos] = useState([]); // State to store fetched courses

  useEffect(() => {
    // Fetch courses when the component mounts
    const fetchCursos = async () => {
      try {
        const response = await axios.get('http://localhost:8082/alumno/cursos');
        setCursos(response.data);
      } catch (error) {
        console.error('Error fetching cursos:', error);
      }
    };

    fetchCursos();
  }, []); // Empty dependency array ensures this effect runs once when the component mounts



  <select name="curso" onChange={handleChange}>
          <option value="" disabled selected>Select a curso</option>
          {cursos.map((curso) => (
            <option key={curso._id} value={curso.nombre}>
              {curso.nombre}
            </option>
*/