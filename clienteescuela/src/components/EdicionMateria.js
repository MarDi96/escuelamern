import React, { useState, useEffect } from "react";
import axios from "axios";

const EdicionMateria = () => {
  const [materias, setMaterias] = useState([]);
  const [error, setError] = useState(null);
  const [editingId, setEditingId] = useState(null);
  const [editedValues, setEditedValues] = useState({});

  useEffect(() => {
    fetchMaterias();
  }, []);

  const fetchMaterias = async () => {
    try {
      const response = await axios.get("/materia");
      setMaterias(response.data);
    } catch (error) {
      setError("Falló la obtención de materias (" + error.message + ")");
      console.error("Error haciendo get de materias: " + error.message);
    }
  };

  const handleEditClick = (id) => {
    setEditingId(id);
    const materiaEdit = materias.find((materia) => materia._id === id);
    setEditedValues(materiaEdit);
  };

  const handleSaveClick = async (id) => {
    try {
      // patch a backend
      await axios.patch(`/materia/${id}`, editedValues);
      setEditingId(null);
      // Refresh 
      fetchMaterias();
    } catch (error) {
      setError(error.message);
    }
  };

  const handleDeleteClick = async (id) => {
    const alertaDelete = window.confirm(
      "Está seguro que quiere borrar esta materia?"
    );

    if (alertaDelete) {
      try {
        await axios.delete(`/materia/${id}`);
        fetchMaterias();
      } catch (error) {
        setError("Falló el borrado de materias (" + error.message + ")");
      }
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditedValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));
  };

  return (
    <div>
      {error && <p>Error: {error}</p>}
      <ul className="ul-sin-bullets">
        {materias.map((materia) => (
          <li key={materia._id}>
            {editingId === materia._id ? ( //render una o la otra basado en que haya una ID, que se refresca al tocar edit
              <div className="formbox">
                Nombre:
                <input
                  type="text"
                  name="nombre"
                  value={editedValues.nombre}
                  onChange={handleInputChange}
                />                
                <button onClick={() => handleSaveClick(materia._id)}>
                  Guardar
                </button>
                <button onClick={() => handleEditClick(null)}>
                Cancelar
              </button>
              </div>
            ) : (           
            <div className="infobox">
              <p>Nombre: {materia.nombre}</p>
              <button onClick={() => handleEditClick(materia._id)}>
                Editar
              </button>
              <button onClick={() => handleDeleteClick(materia._id)}>
                Borrar
              </button>
            </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default EdicionMateria;
