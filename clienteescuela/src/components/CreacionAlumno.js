import React, { useState, useEffect } from "react";
import axios from "axios";

const CreacionAlumno = () => {
  const [cursos, setCursos] = useState([]);
  const [materias, setMaterias] = useState([]);
  const [error, setError] = useState(null);
  const [formData, setFormData] = useState({
    nombre: "",
    contacto: "",
    curso: "",
    dni: ""
  });

  useEffect(() => {
    fetchCursos();
    fetchMaterias();
  }, []);

  useEffect(() => {
    if (formData.curso) {
      fetchMateriasPorCurso(formData.curso);
    }
  }, [formData.curso]);

  const fetchMateriasPorCurso = async (cursoId) => {
    try {
      const response = await axios.get(`/curso/${cursoId}/materias`);
      console.log("J<KASHDFKJSGDTFJKDSA" + response.data);
      setMaterias(response.data);
    } catch (error) {
      console.error('Error haciendo get de materias:', error);
    }
  };

  const fetchCursos = async () => {
    try {
      const response = await axios.get("/curso");
      setCursos(response.data);
    } catch (error) {
      setError("Falló la obtención de cursos (" + error.message + ")");
      console.error("Error haciendo get de cursos: " + error.message);
    }
  };

  const fetchMaterias = async () => {
    try {
      const response = await axios.get("/materia");
      setMaterias(response.data);
    } catch (error) {
      setError("Falló la obtención de materias (" + error.message + ")");
      console.error("Error haciendo get de materias: " + error.message);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;

    // si cambia el curso, nuevo fetch
    if (name === 'curso') {
      setFormData({ ...formData, [name]: value, notas: [] });
    } else {
      setFormData({ ...formData, [name]: value });
    }
  };

  
  const registroAlumno = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('/alumno', {
        ...formData,
        notas: materias.map((materia) => materia._id)});

      console.log('Registro completo:', response.data);
    } catch (error) {
      console.error('Error de registro:', error);
    }
  };


  return (
    <div>
      <form className="formbox" onSubmit={registroAlumno}>
        <h1>Registro de alumno</h1>
        <input
          type="text"
          name="nombre"
          className="textbox"
          onChange={handleChange}
          placeholder="Nombre"
        />
        <input
          type="text"
          name="dni"
          className="textbox"
          onChange={handleChange}
          placeholder="DNI"
        />
        <input
          type="text"
          name="contacto"
          className="textbox"
          onChange={handleChange}
          placeholder="email"
        />
        <label>
          Curso:
          <select
            name="curso"
            value={formData.curso}
            onChange={handleChange}
          >
            <option value=""></option>
            {cursos.map((curso) => (
              <option key={curso._id} value={curso._id}>
                {curso.nombre}
              </option>
            ))}
          </select>
        </label>
        <button type="submit" className="botonregistrar">
          Registrar
        </button>
      </form>
    </div>
  );
};

export default CreacionAlumno;
