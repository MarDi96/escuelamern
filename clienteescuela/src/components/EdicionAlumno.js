import React, { useState, useEffect } from "react";
import axios from "axios";

const EdicionAlumno = () => {
  const [alumnos, setAlumnos] = useState([]);
  const [cursos, setCursos] = useState([]);
  const [error, setError] = useState(null);
  const [editingId, setEditingId] = useState(null);
  const [editedValues, setEditedValues] = useState({});

  useEffect(() => {//cada vez que un state cambie refresco
    fetchAlumnos();
    fetchCursos();
  }, []); // el array vacío para que no haya infinite loop

  const fetchAlumnos = async () => {
    try {// get a backend
      const response = await axios.get("/alumno");
      setAlumnos(response.data);
    } catch (error) {
      setError(error.message);
    }
  };

  const fetchCursos = async () => {
    try {
      const response = await axios.get("/curso");
      setCursos(response.data);
    } catch (error) {
      setError("Falló la obtención de cursos (" + error.message + ")");
      console.error("Error haciendo get de cursos: " + error.message);
    }
  };

  const handleEditClick = (id) => {
    setEditingId(id);
    const alumnoEdit = alumnos.find((alumno) => alumno._id === id);
    setEditedValues(alumnoEdit);
  };

  const handleSaveClick = async (id) => {
    try {
      // patch a backend
      await axios.patch(`/alumno/${id}`, editedValues);
      setEditingId(null);
      // Refresh 
      fetchAlumnos();
    } catch (error) {
      setError(error.message);
    }
  };

  // voy llenando el objeto con nombre y valor de lo que cambie
  const handleInputChange = (e) => {
    const { name, value } = e.target;
 
    const newvalues = {...editedValues};
    newvalues[name] = value;
    setEditedValues(newvalues);/*
    setEditedValues((prevValues) => ({
      ...prevValues,
      [name]: value,
    }));*/
 
  };

  return (
    <div>
      {error && <p>Error: {error}</p>}
      <ul className="ul-sin-bullets">
        {alumnos.map((alumno) => ( //hago el mapa para llenar la lista
          <li key={alumno._id}>
            {editingId === alumno._id ? (
              <div className="formbox">
                <input
                  type="text"
                  name="nombre"
                  value={editedValues.nombre}
                  onChange={handleInputChange}
                />
                <input
                  type="text"
                  name="dni"
                  value={editedValues.dni}
                  onChange={handleInputChange}
                />
                 <input
                  type="text"
                  name="contacto"
                  value={editedValues.contacto}
                  onChange={handleInputChange}
                />
                <select
                  name="curso"
                  value={editedValues.curso}
                  onChange={handleInputChange}>
                  {cursos.map((curso) => (
                    <option key={curso._id} value={curso._id}>
                      {curso.nombre}
                    </option>
                  ))}
                </select>
                
                <button onClick={() => handleSaveClick(alumno._id)}>
                  Guardar
                </button>
                <button onClick={() => handleEditClick(null)}>
                Cancelar
              </button>
              </div>
            ) : (
              <div className="infobox">
                <p>Nombre: {alumno.nombre}</p>
                <p>DNI: {alumno.dni}</p>
                <p>Contacto: {alumno.contacto}</p>
                <p>Curso: {alumno.curso && alumno.curso.nombre}</p>
                <button onClick={() => handleEditClick(alumno._id)}>
                  Editar
                </button>
              </div>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default EdicionAlumno;
