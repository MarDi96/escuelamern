import React, { useState, useEffect } from "react";
import axios from "axios";

const NotasAlumno = () => {
  const [alumnos, setAlumnos] = useState([]);
  const [error, setError] = useState(null);
  const [editingId, setEditingId] = useState(null);
  const [editedNotas, setEditedNotas] = useState({});

  useEffect(() => {
    fetchAlumnos();
  }, []);

  const fetchAlumnos = async () => {
    try {
      const response = await axios.get("/alumno");
      setAlumnos(response.data);
    } catch (error) {
      setError(error.message);
    }
  };

  const handleEditClick = (id) => {
    setEditingId(id);

    const alumnoEdit = alumnos.find((alumno) => alumno._id === id);
    setEditedNotas(
      alumnoEdit.notas.reduce((notasObj, nota) => {
        notasObj[nota.materia._id] = nota.calificacion;
        return notasObj;
      }, {}),
    );
  };

  const handleSaveClick = async (id) => {
    try {
        const alumnoEdit = alumnos.find((alumno) => alumno._id === id);
        console.log("vamos a identificar bien " + JSON.stringify(alumnoEdit, null, 2));

      // patch a backend
      await axios.patch(`/alumno/${id}`, { notas: editedNotas });
      setEditingId(null);
      // Refresh
      fetchAlumnos();
    } catch (error) {
      setError(error.message);
    }
  };

  // voy llenando el objeto con nombre y valor de lo que cambie
  const handleInputChange = (e, materiaId) => {
    const { value } = e.target;
    setEditedNotas((prevNotas) => ({
      ...prevNotas,
      [materiaId]: value,
    }));
    console.log("print de input change "+ JSON.stringify(editedNotas,null,2));
  };

  return (
    <div>
      {error && <p>Error: {error}</p>}
      <ul className="ul-sin-bullets">
        {alumnos.map((alumno) => (
          <li key={alumno._id}>
            <div className="infobox">
              <p>Nombre: {alumno.nombre}</p>
              <p>DNI: {alumno.dni}</p>
              <p>Contacto: {alumno.contacto}</p>
              <p>Curso: {alumno.curso && alumno.curso.nombre}</p>
              <div>
                <p>Notas:</p>
                <ul>
                  {alumno.notas.map((nota) => (
                    <li key={nota.materia._id}>
                      {nota.materia.nombre}:&nbsp;
                      {editingId === alumno._id ? (
                        <input
                          type="text"
                          value={editedNotas[nota.materia._id] || ""}
                          onChange={(e) =>
                            handleInputChange(e, nota.materia._id)
                          }
                        />
                      ) : (
                        <span>{nota.calificacion}</span>
                      )}
                    </li>
                  ))}
                </ul>
              </div>
              {editingId === alumno._id && (
                <div className="formbox">
                  <button onClick={() => handleSaveClick(alumno._id)}>
                    Guardar
                  </button>
                  <button onClick={() => setEditingId(null)}>Cancelar</button>
                </div>
              )}
              {editingId !== alumno._id && (
                <button onClick={() => handleEditClick(alumno._id)}>
                  Editar Notas
                </button>
              )}
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default NotasAlumno;
