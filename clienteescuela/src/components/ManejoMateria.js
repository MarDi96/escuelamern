
import React, {useState} from 'react';
import Header from './Header';
import CreacionMateria from './CreacionMateria';
import EdicionMateria from './EdicionMateria';

const ManejoMateria = () => {
    const [showCrear, setShowCrear] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    
    return(
        <div>
          <Header />
          <button onClick={() => setShowCrear(!showCrear)}>
            Crear nueva materia
          </button>
          
    
          <button onClick={() => setShowEdit(!showEdit)}>
            Lista de materias
          </button>
          
    
          <div>
          {showCrear ? <CreacionMateria /> : null}
          {showEdit ? <EdicionMateria /> : null}
          </div>
        </div>
    );
}

export default ManejoMateria;