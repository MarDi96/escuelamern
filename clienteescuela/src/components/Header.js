//Header.js
import React from "react";
import { Routes, Route, useNavigate, BrowserRouter as Router, Link } from "react-router-dom";


const Header = () => {
  const navigate = useNavigate();
  const navigateHome = () => {
    // 👇️ navigate to /
    navigate("/");
  };

  const navigateManejoAlumno = () => {
    // 👇️ navigate to /alumnos (manejoalumnos)
    navigate("/alumnos");
  };

  const navigateManejoCurso = () => {
    // 👇️ navigate to /cursos (manejocursos)
    navigate("/cursos");
  };

  const navigateManejoMateria = () => {
    // 👇️ navigate to /materias (manejomaterias)
    navigate("/materias");
  };

  return (
    <header>
      <nav className="headernav">
        
        <ul className="nav-items">
          <li onClick={navigateHome}>Home</li>
          <li onClick={navigateManejoAlumno}>Alumnos</li>
          <li onClick={navigateManejoCurso}>Cursos</li>
          <li onClick={navigateManejoMateria}>Materias</li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;

/*<img src="https://images.rawpixel.com/image_png_800/cHJpdmF0ZS9sci9pbWFnZXMvd2Vic2l0ZS8yMDIyLTExL3JtNjAzLWVsZW1lbnQtMTg2LnBuZw.png" className="nav-logo"/>*/
