import React, {useState, useEffect} from 'react';
import axios from 'axios';

const CreacionCurso = () => {
    const [nombre, setNombre] = useState('');
    const [materias, setMaterias] = useState([]);
    const [selectedMaterias, setSelectedMaterias] = useState([]);
    const [error, setError] = useState(null);
  
    useEffect(() => {
  
      fetchMaterias();
    }, []); // array de dependencia vacío para evitar un loop

    const fetchMaterias = async () => {
      try {
        const response = await axios.get('/materia');
        setMaterias(response.data);
      } catch (error) {
        setError('Falló la obtención de materias ('+ error.message +')');
        console.error('Error haciendo get de materias: '+ error.message);         
      }
    };
  
    const handleFormSubmit = async (e) => {
      e.preventDefault();
      setError('');
      try {
        const response = await axios.post('/curso', {
          nombre: nombre,
          materias: selectedMaterias,
        });
  
        console.log('Nuevo Curso: ', response.data);
  
        // Reset form
        setNombre('');
        setSelectedMaterias([]);
      } catch (error) {
        setError('Error de creacion ('+ error.message +')');
        console.error('Error creando curso: ', error.message);
      }
    };
    return(
        <div>
   
        <form onSubmit={handleFormSubmit} className='curso--registro-formbox'>
        <h1>Registro de Curso</h1>
        {error && <p style={{ color: 'red' }}>{error}</p>}
          <label className='curso--nombre-box'>
            Nombre:
            <input
              type="text"
              value={nombre}
              onChange={(e) => setNombre(e.target.value)}
            />
          </label>
          <div>
          <label>
            Materias:
            <select
              multiple
              value={selectedMaterias}
              onChange={(e) =>
                setSelectedMaterias(Array.from(e.target.selectedOptions, (option) => option.value))
              }
            >
              {materias.map((materia) => (
                <option key={materia._id} value={materia.nombre}>
                  {materia.nombre}
                </option>
              ))}
            </select>
          </label>
          <button type="submit" className='curso--botonsubmit'>Crear</button>
          </div>

          
            
          
        </form>
      </div>
    );
        
}

export default CreacionCurso;