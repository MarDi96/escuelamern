// Import necessary modules and components
import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import axios from "axios";
import "./index.css";
import Login from "./components/login";
import Profile from "./components/Profile";


axios.defaults.baseURL = "http://localhost:8082/";

const App = () => {
  const [user, setUser] = useState({ auth: false, username: "" });
  return (
    <div className="App">
      {user.auth ? (
            <Profile user={user} setUser={setUser} />
          ) : (
            <Login setUser={setUser} />
          )}
    </div>
  );
};
export default App;

/*<Route exact path="*" element={<Home />} />*/