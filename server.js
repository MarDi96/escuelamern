// server.js
require('dotenv').config();
const express = require('express');
const connectDB = require('./config/db');
const bodyParser = require('body-parser');
const cors = require('cors');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const alumnoRoutes = require('./routes/api/alumnoRoutes');
const cursoRoutes = require('./routes/api/cursoRoutes');
const materiaRoutes = require('./routes/api/materiaRoutes');
const { authenticate } = require('./middlewares/auth');

const router = express.Router();

router.get('/profile', authenticate, (req, res) => {
  res.json({ message: `Welcome ${req.user.username}` });
});

module.exports = router;

const app = express();
const PORT = process.env.PORT || 8082;

connectDB();
// cors
app.use(cors({ origin: true, credentials: true }));

// Middleware
app.use(bodyParser.json());

// Auth routes
app.use('/auth', authRoutes);

// User routes
app.use('/user', userRoutes);

// api routes
app.use('/alumno', alumnoRoutes);
app.use('/curso', cursoRoutes);
app.use('/materia', materiaRoutes);

// Routes
app.get('/', (req, res) => {
  res.send('School Management System API');
});


// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
