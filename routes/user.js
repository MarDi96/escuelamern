const express = require('express');
const { authenticate, updatePassword } = require('../middlewares/auth');

const router = express.Router();

router.get('/profile', authenticate, (req, res) => {
  res.json({ message: `Welcome ${req.user.username}` });
});

router.post('/update', authenticate, updatePassword);

module.exports = router;