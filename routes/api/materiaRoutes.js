// routes/materia.js
const express = require('express');
const router = express.Router();
const Materia = require('../../models/materia');
const Alumno = require('../../models/alumno');
const Curso = require('../../models/curso');

// new materia
router.post('/', async (req, res) => {
  try {
    const newMateria = await Materia.create(req.body);
    res.status(201).json(newMateria);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Get all materias
router.get('/', async (req, res) => {
  try {
    const materias = await Materia.find();
    res.status(200).json(materias);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Get by ID
router.get('/:id', async (req, res) => {
  try {
    const materia = await Materia.findById(req.params.id);
    if (!materia) {
      return res.status(404).json({ error: 'Materia no encontrada' });
    }
    res.status(200).json(materia);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Update by ID
router.patch('/:id', async (req, res) => {
  try {
    const updatedMateria = await Materia.findByIdAndUpdate(req.params.id, req.body, { new: true });
    if (!updatedMateria) {
      return res.status(404).json({ error: 'No se encuentra la materia a actualizar' });
    }

    await Alumno.updateMany(
      { 'notas.materia': req.params.id },
      { $set: { 'notas.$.materia': updatedMateria } }
    );
    
    res.status(200).json(updatedMateria);
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Delete by ID
router.delete('/:id', async (req, res) => {
  try {
    const deletedMateria = await Materia.findByIdAndDelete(req.params.id);
    if (!deletedMateria) {
      return res.status(404).json({ error: 'No se encuentra la materia a borrar' });
    }

    await Alumno.updateMany(
      { 'notas.materia': req.params.id },
      { $pull: { 'notas': { materia: req.params.id } } }
    );

    await Curso.updateMany(
      { materias: req.params.id },
      { $pull: { materias: req.params.id } }
    );
    res.status(200).json({ message: 'Materia borrada' });
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = router;
