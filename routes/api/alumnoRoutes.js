// routes/alumnoRoutes.js
const express = require('express');
const router = express.Router();
const Alumno = require('../../models/alumno');
const Materia = require('../../models/materia');
const Curso = require('../../models/curso');

// Get all students
router.get('/', async (req, res) => {
  try {
    const alumnos = await Alumno.find()
    .populate('curso')
    .populate('notas.materia');
    res.json(alumnos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


// Create a student
router.post('/', async (req, res, next) => {
  //si no hay alumnos
  req.body.legajo = 1;
  //si ya había alguno
  const ultimoAlumno = await Alumno.find({}, {legajo:1}).sort({_id: -1}).limit(1);
  if(ultimoAlumno.length!==0){
    req.body.legajo = ultimoAlumno[0].legajo + 1;
  }

  try{
    const existeCurso = await Curso.findById(req.body.curso);
    
    if (!existeCurso){
    return res.status(400).json({error: 'no existe ese curso'});
  }

  const notasArray = existeCurso.materias.map((materia) => ({
  materia: materia._id,
  calificacion: 0
  }));
 
  const alumno = new Alumno({
    nombre: req.body.nombre,
    contacto: req.body.contacto,
    curso: req.body.curso,
    dni: req.body.dni,
    legajo: req.body.legajo,
    notas: notasArray
  });

  try {
    const newAlumno = await alumno.save();
    res.status(201).json(newAlumno);
  }catch (error) {
    res.status(400).json({ message: error.message });
    next(error);
  }
} catch (error) {
  next(error);
}
});




async function getAlumno(req, res, next) {
  try {
    const alumno = await Alumno.findOne().sort()
    if (alumno == null) {
      return res.status(404).json({ message: 'Alumno no encontrado' });
    }
    res.alumno = alumno;
    next();
  } catch (error) {
    return res.status(500).json({ message: error.message });
  }
}

// Get one student
router.get('/:id', getAlumno, (req, res) => {
  res.json(res.alumno);
});

// Delete a student
router.delete('/:id', getAlumno, async (req, res) => {
  try {
    await res.alumno.remove();
    res.json({ message: 'Alumno borrado' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


// Update a student
router.patch('/:id', getAlumno, async (req, res) => {
 if (req.body.nombre != null) {
    res.alumno.nombre = req.body.nombre;
  }

  if (req.body.dni != null) {
    res.alumno.dni = req.body.dni;
  }

  if (req.body.contacto != null) {
    res.alumno.contacto = req.body.contacto;
  }

  if (req.body.curso != null) {
    res.alumno.curso = req.body.curso;
  }

  try{
    const existeCurso = await Curso.findById(req.body.curso);
 
    res.alumno.notas = existeCurso.materias.map((materia) => ({

      materia: materia,
      calificacion: 0
      }));  
    if (!existeCurso){
    return res.status(400).json({error: 'no existe ese curso'});
  }}catch(error){console.log(error);}





try {
  const updatedAlumno = await res.alumno.save();
  res.json(updatedAlumno);
} catch (error) {
  console.log(error);
  res.status(400).json({ message: error.message });
}
});

module.exports = router;


/*
  if (req.body.notas != null) {
    console.log("xd "+ JSON.stringify(req.body.notas ,null,2));
    res.alumno.notas = Object.values(req.body.notas).map(x => {
      console.log("materiaid be like "+ Object.entries(req.body.notas));
      return{
      materia: x.materia._id,
      calificacion: Number(x.calificacion)|| 0}
      });
    
  }else res.alumno.notas = [];
  */