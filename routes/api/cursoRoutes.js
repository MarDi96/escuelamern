const express = require("express");
const router = express.Router();
const Curso = require("../../models/curso");
const Materia = require("../../models/materia");
const Alumno = require("../../models/alumno");

// get all cursos
router.get("/", async (req, res) => {
  try {
    const cursos = await Curso.find().populate("materias");
    res.json(cursos);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// Nuevo curso
router.post("/", async (req, res) => {
  try {
    const { nombre, materias } = req.body;
    const existingMaterias = await Materia.find({ _id: { $in: materias } });
    if (existingMaterias.length !== materias.length) {
      return res
        .status(400)
        .json({ error: "One or more materias do not exist" });
    }

    const nuevoCurso = await Curso.create({ nombre, materias });
    res.status(201).json(nuevoCurso);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

// Curso por ID
router.get("/:id", async (req, res) => {
  try {
    const curso = await Curso.findById(req.params.id);
    if (!curso) {
      return res.status(404).json({ message: "Ese curso no existe" });
    }
    res.json(curso);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// patch by ID
router.patch("/:id", async (req, res) => {
  try {
    const curso = await Curso.findByIdAndUpdate(
      req.params.id,
      {
        $set: {
          nombre: req.body.nombre,
          materias: req.body.materias,
        },
      },
      { new: true }
    );

    if (!curso) {
      return res.status(404).json({ message: "Ese curso no existe" });
    }

    // Update references
    await Alumno.updateMany(
      { curso: req.params.id },
      { $set: { curso: curso } }
    );

    res.json(curso);
  } catch (error) {
    console.log("error de patcheo de curso " + error);
    res.status(500).json({ message: error.message });
  }
});

// delete by ID
router.delete("/:id", async (req, res) => {
  try {
    const curso = await Curso.findByIdAndRemove(req.params.id);
    if (!curso) {
      return res.status(404).json({ message: "Ese curso no existe" });
    }

    // Remove references in Alumnos
    await Alumno.updateMany({ curso: req.params.id }, { $unset: { curso: 1 } });

    res.json({ message: "Curso borrado" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

// get materias
router.get("/:id/materias", async (req, res, next) => {
  try {
    const curso = await Curso.findById(req.params.id).populate("materias");
    if (!curso) {
      return res.status(404).json({ message: "Ese curso no existe" });
    }
    res.json(curso.materias);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
