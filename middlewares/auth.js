
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('config');
const key = config.get('SECRET_KEY');
const bcrypt = require('bcrypt');

// token verifier
const authenticate = async (req, res, next) => {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ message: 'Authentication required' });
  }

  try {
    const decodedToken = jwt.verify(token, key);
    const user = await User.findById(decodedToken.userId);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    req.user = user;
    next();
  } catch (error) {
    res.status(401).json({ message: 'Invalid token' });
  }
};

// update password
const updatePassword = async(req,res)=> {
  try{
    const { oldPassword, newPassword } = req.body
    const { passwordHash } = process.env.PASSWORD_HASH
    if(oldPassword && newPassword) {
      let match = await bcrypt.compare(oldPassword.plaintext, passwordHash)
      if(match) {
        let hash = await bcrypt.hash(newPassword, saltRounds)
        return res.sendStatus(200)
      }
    }
    return res.sendStatus(401)
  } catch(err){
    console.log(err)
    return res.sendStatus(500)
  }
}

module.exports = { authenticate, updatePassword };