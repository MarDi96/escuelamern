// models/alumno.js
const mongoose = require('mongoose');

const alumnoSchema = new mongoose.Schema({
  nombre: { type: String, required: true },
  dni:  { type: Number, required: true},
  legajo:  { type: Number, required: true},
  contacto: { type: String, required: true },
  notas: [{
    materia: { type: mongoose.Schema.Types.ObjectId, ref: 'Materia'},
    calificacion: {type: Number}}],
  curso: { type: mongoose.Schema.Types.ObjectId, ref: 'Curso', required: true }
}
);

module.exports = mongoose.model('Alumno', alumnoSchema);
