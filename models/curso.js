// models/curso.js
const mongoose = require('mongoose');

const cursoSchema = new mongoose.Schema({
  nombre: { type: String, required: true, unique: true },
  materias: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Materia' }]
}
  );

module.exports = mongoose.model('Curso', cursoSchema);

//  materias: { type: [String], ref: 'Materia', required: true }

//  materias: { type: mongoose.Schema.Types.ObjectId, ref: 'Materia' }